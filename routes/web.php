<?php
use App\Http\Controllers\UserController;
use App\Http\Controllers\OrganController;
use Illuminate\Support\Facades\Route;
use App\Models\Organ;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home',['title'=>'Home']);
})->name('home');
Route::get('/page2', function () {
    return view('page2');
})->name('page2');

Route::get('/register', [UserController::class,'register'])->name('register');
Route::post('/register', [UserController::class,'register_action'])->name('register.action');
Route::get('/login', [UserController::class,'login'])->name('login');
Route::post('/login', [UserController::class,'login_action'])->name('login.action');
Route::get('/password', [UserController::class,'password'])->name('password');
Route::post('/password', [UserController::class,'password_action'])->name('password.action');
Route::get('/logout', [UserController::class,'logout'])->name('logout');
Route::get('/index', [OrganController::class,'operateDB'])->name('index');
Route::get('/page1', [OrganController::class,'page1'])->name('page1');
// Route::get('/page1', [OrganController::class,'page1_action'])->name('page1.action');

