<?php

namespace App\Http\Controllers;

use App\Models\Organ;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrganController extends Controller
{
    function operateDB(){
        $data = DB::table('tb_organ')->get();
        return view('member.index',['tb_organ'=>$data]);
    }

    // public function index(Request $request){
    //     $data['title'] = 'Organ';
    //     $data['q'] = $request->get('q');
    //     $data['tb_organ'] = Organ::where('name','like','%'.$data['q'].'%')->get();
    //     return view('organ.index',$data);
    // }
    public function page1(){
        
        return view('member/page1');
    }
    public function page1_action(Request $request){
        $request->validate([
            'detail' => 'required',
            'history' => 'required'
        ]);
        // $organ = new Organ([
        //     'detail' => $request->organ_name
        // ]);
        // $user->save();
        return redirect()->route('page1')->with('success','Welcome page 2.');
    }
}
