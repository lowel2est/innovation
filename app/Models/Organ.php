<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Organ extends Model
{
    use HasFactory;

    protected $table = 'tb_organ';
    protected $primaryKey = 'organ_id';
    protected $fillable = ['organ_name','created_at','updated_at'];

}
