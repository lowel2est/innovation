<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cstudy extends Model
{
    use HasFactory;

    protected $table = 'tb_cstudy';
    protected $primaryKey = 'cstudy';
    protected $fillable = ['cstudy_name','created_at','updated_at'];

}
