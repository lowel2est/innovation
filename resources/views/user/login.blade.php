@extends('app')
@section('content')
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4 border border-5">
        @if(session('success'))
        <p class="alert alert-success">{{session('success')}}</p>
        @endif
        @if($errors->any())
        @foreach($errors->all() as $err)
        <p class="alert alert-danger">{{$errors}}</p>
        @endforeach
        @endif
        <form method="POST" class="mt-4 text-center" action="{{route('login.action')}}">
            @csrf
            <!-- <img class="mb-4" src="/docs/5.0/assets/brand/bootstrap-logo.svg" alt="" width="72" height="57"> -->
            <img class="mb-0" src="{!! url('images/logo.png') !!}" alt="" width="295" height="250.5">
            <h1 class="h3 mb-5 fw-normal text-center">Please sign in</h1>
            <div class="mt-1 form-floating">
                <!-- <label>Username <span class="text-danger">*</span></label> -->

                <div class="form-group form-floating mb-1">
                    <input type="text" class="form-control" name="username" value="{{ old('username') }}"
                        placeholder="Username" required="required" autofocus>
                    <label for="floatingName">Email or Username</label>
                    @if ($errors->has('username'))
                    <span class="text-danger text-left">{{ $errors->first('username') }}</span>
                    @endif
                </div>

                <!-- <input class="form-control" type="text" id="floatingInput" name="username"
                        value="{{old('username')}}" />
                    <label for="floatingInput">Username</label> -->
            </div>


            <div class="mt-1 form-floating">
                <!-- <label>Password <span class="text-danger">*</span></label> -->

                <div class="form-group form-floating mb-1">
                    <input type="password" class="form-control" name="password" value="{{ old('password') }}"
                        placeholder="Password" required="required">
                    <label for="floatingPassword">Password</label>
                    @if ($errors->has('password'))
                    <span class="text-danger text-left">{{ $errors->first('password') }}</span>
                    @endif
                </div>

                <!-- <input class="form-control" id="floatingPassword" type="password" name="password" />
                    <label for="floatingPassword">Password</label> -->
            </div>
            
                <button class="mt-1z w-100 btn btn-lg btn-primary">Login</button></br>
                <!-- <a class="mt-1 w-100 btn btn-lg btn-danger" href="{{route('home')}}">back</a> -->
            
            <!-- <p class="mb-4 text-muted">© 2022-2023</p> -->
            <p class="mt-5"></p>
        </form>
    </div>
    <div class="col-md-4"></div>
</div>
@endsection