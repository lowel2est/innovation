@extends('app')
@section('content')
@auth
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4 border border-5">
        @if(session('success'))
        <p class="alert alert-success">{{session('success')}}</p>
        @endif
        @if($errors->any())
        @foreach($errors->all() as $err)
        <p class="alert alert-danger">{{$err}}</p>
        @endforeach
        @endif
        <form method="POST" class="mt-4 text-center" action="{{route('password.action')}}">
            @csrf
            <img class="mb-0" src="{!! url('images/logo.png') !!}" alt="" width="295" height="250.5">
            <h1 class="h3 mb-4 fw-normal">Change Password</h1>
            <div class="form-group form-floating mb-1">
                <input type="password" class="form-control" name="old_password" value="{{ old('old_password') }}"
                    placeholder="Confirm Password" required="required">
                <label for="floatingConfirmPassword">Confirm Password</label>
                @if ($errors->has('old_password'))
                <span class="text-danger text-left">{{ $errors->first('old_password') }}</span>
                @endif
            </div>
            <div class="form-group form-floating mb-1">
                <input type="password" class="form-control" name="new_password" value="{{ old('new_password') }}"
                    placeholder="Confirm Password" required="required">
                <label for="floatingConfirmPassword">New Password</label>
                @if ($errors->has('new_password'))
                <span class="text-danger text-left">{{ $errors->first('new_password') }}</span>
                @endif
            </div>
            <div class="form-group form-floating mb-1">
                <input type="password" class="form-control" name="new_password_confirmation"
                    value="{{ old('new_password_confirmation') }}" placeholder="Confirm Password" required="required">
                <label for="floatingConfirmPassword">Confirm Password</label>
                @if ($errors->has('new_password_confirmation'))
                <span class="text-danger text-left">{{ $errors->first('new_password_confirmation') }}</span>
                @endif
            </div>

            <button class="mt-0 w-100 btn btn-lg btn-primary">Change Password</button>
            <!-- <a class="btn btn-danger" href="{{route('home')}}">back</a> -->

            <p class="mt-5"></p>
        </form>
    </div>
    <div class="col-md-4"></div>
</div>
@endauth
@endsection