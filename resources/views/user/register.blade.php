@extends('app')
@section('content')

<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4 border border-5">
        @if($errors->any())
        @foreach($errors->all() as $err)
        <p class="alert alert-danger">{{$err}}</p>
        @endforeach
        @endif
        <form method="POST" class="mt-4 text-center" action="{{route('register.action')}}">
            @csrf
            <img class="mb-0" src="{!! url('images/logo.png') !!}" alt="" width="295" height="250.5">
            <h1 class="h3 mb-5 fw-normal text-center">Please Register</h1>


            <div class="form-group form-floating mb-1">
                <input type="text" class="form-control" name="name" value="{{ old('email') }}"
                    placeholder="name@example.com" required="required" autofocus>
                <label for="floatingEmail">Email address</label>
                @if ($errors->has('email'))
                <span class="text-danger text-left">{{ $errors->first('email') }}</span>
                @endif
            </div>

            <div class="form-group form-floating mb-1">
                <input type="text" class="form-control" name="username" value="{{ old('username') }}"
                    placeholder="Username" required="required" autofocus>
                <label for="floatingName">Username</label>
                @if ($errors->has('username'))
                <span class="text-danger text-left">{{ $errors->first('username') }}</span>
                @endif
            </div>

            <div class="form-group form-floating mb-1">
                <input type="password" class="form-control" name="password" value="{{ old('password') }}"
                    placeholder="Password" required="required">
                <label for="floatingPassword">Password</label>
                @if ($errors->has('password'))
                <span class="text-danger text-left">{{ $errors->first('password') }}</span>
                @endif
            </div>

            <div class="form-group form-floating mb-1">
                <input type="password" class="form-control" name="password_confirmation"
                    value="{{ old('password_confirmation') }}" placeholder="Confirm Password" required="required">
                <label for="floatingConfirmPassword">Confirm Password</label>
                @if ($errors->has('password_confirmation'))
                <span class="text-danger text-left">{{ $errors->first('password_confirmation') }}</span>
                @endif
            </div>
            <button class="mt-0 w-100 btn btn-lg btn-primary" type="submit">Register</button>
            <!-- <p class="mb-4 text-muted">© 2022-2023</p> -->
            <p class="mt-5"></p>
        </form>
    </div>
    <div class="col-md-4"></div>
</div>

@endsection