@extends('app')
@section('content')
@auth

<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4 border border-5">
        @if($errors->any())
        @foreach($errors->all() as $err)
        <p class="alert alert-danger">{{$err}}</p>
        @endforeach
        @endif
        <form method="POST" class="mt-4 text-center" action="">
            @csrf
            <!-- <img class="mb-0" src="{!! url('images/logo.png') !!}" alt="" width="295" height="250.5"> -->
            <h1 class="h3 mb-5 fw-normal text-center">ขั้นที่ 1 ตรวจร่างกาย</h1>


            <div class="form-group form-floating mb-1">
            <!-- <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea> -->
                <input type="text" class="form-control" name="detail" value="{{ old('detail') }}"
                    placeholder="name@example.com" required="required" autofocus>
                <label for="floatingEmail">Detail</label>
                @if ($errors->has('detail'))
                <span class="text-danger text-left">{{ $errors->first('detail') }}</span>
                @endif
            </div>

            <div class="form-group form-floating mb-1">
                <input type="text" class="form-control" name="history" value="{{ old('history') }}"
                    placeholder="history" required="required" autofocus>
                <label for="floatingName">History</label>
                @if ($errors->has('history'))
                <span class="text-danger text-left">{{ $errors->first('history') }}</span>
                @endif
            </div>

            <button class="mt-0 w-100 btn btn-lg btn-primary" type="submit">Record</button>
            <!-- <p class="mb-4 text-muted">© 2022-2023</p> -->
            <p class="mt-5"></p>
        </form>
    </div>
    <div class="col-md-4"></div>
</div>
@endauth
@endsection