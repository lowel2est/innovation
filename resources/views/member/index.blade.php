@extends('app')
@section('content')
@auth
@php
  $counter = 1;
@endphp
@foreach ($tb_organ as $organ)
<div class="row mt-1">
    <div class="col-md-2"></div>
    <div class="col-md-3 me-1 border border-5">
        @if($counter % 2 == 0)
        <a class="w-100 h-100 btn btn-outline-info btn btn-lg" href="{{route('page1')}}"><b>{{$organ->organ_name}}</b></a>
        @else
        <a class="w-100 h-100 btn btn-outline-primary btn btn-lg" href="{{route('page1')}}"><b>{{$organ->organ_name}}</b></a>
        @endif
    </div>
    <div class="col-md-5 border border-5">
        <div class="card-body">
            <h5 class="card-title">{{$organ->organ_name}}</h5>
            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to
                additional content. This content is a little bit longer.</p>
            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>
@php 
   $counter++;
  @endphp
@endforeach
@endauth
<!-- <header class="bg-dark text-white mt-1">
    <div class="d-flex flex-wrap align-items-center justify-content-start">

        <p class="mt-3">© 2022-2023</p>

    </div>
</header> -->
@endsection