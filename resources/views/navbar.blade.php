<header class="p-3 bg-dark text-white">

    <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
        <a href="/" class="d-flex align-items-center mb-lg-0 text-white text-decoration-none">
            <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap">
                <use xlink:href="#bootstrap"></use>
            </svg>
        </a>

        <ul class="nav col-12 col-lg-auto me-lg-auto justify-content-center mb-md-0">
            <li><a href="{{route('home')}}" class="nav-link px-2 text-secondary">Home</a></li>
            <li><a href="#" class="nav-link px-2 text-white">Features</a></li>
            <li><a href="#" class="nav-link px-2 text-white">Pricing</a></li>
            <li><a href="#" class="nav-link px-2 text-white">FAQs</a></li>
            <li><a href="#" class="nav-link px-2 text-white">About</a></li>
        </ul>

        <div class="text-end">
            @auth
            <!-- <p>ยินดีต้อนรับ <b>{{Auth::user()->name}}</b> </p> -->

            <a class="btn btn-success" href="{{route('index')}}">ยินดีต้อนรับ <b>{{Auth::user()->name}}</b></a>
            <a class="btn btn-primary" href="{{route('password')}}">Change Password</a>
            <a class="btn btn-danger" href="{{route('logout')}}">Logout</a>
            @endauth
            @guest
            <a class="btn btn-primary" href="{{route('login')}}">Login</a>
            <a class="btn btn-info" href="{{route('register')}}">Register</a>
            @endguest
        </div>
    </div>

</header>